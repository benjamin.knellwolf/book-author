package ch.bict.book_author.service;

import ch.bict.book_author.model.Author;
import ch.bict.book_author.model.Book;
import ch.bict.book_author.repository.AuthorRepository;
import ch.bict.book_author.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.*;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    public List<Book> getBooks() {
        return this.bookRepository.findAll();
    }

    public Book getBookById(Long id) {
        return this.bookRepository.findById(id).orElse(null);
    }

    public Book saveBook(@Valid Book book) {
        return this.bookRepository.saveAndFlush(book);
    }

    public void deleteBookById(Long id) {

        Book book = this.bookRepository.getOne(id);

        for (Author author : this.authorRepository.findAll()) {
            author.getBooks().remove(book);
            this.authorRepository.save(author);
        }
        this.authorRepository.flush();
        this.bookRepository.deleteById(id);
    }
}
