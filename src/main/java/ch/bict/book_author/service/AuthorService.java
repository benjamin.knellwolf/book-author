package ch.bict.book_author.service;

import ch.bict.book_author.model.Author;
import ch.bict.book_author.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.validation.Valid;
import java.util.*;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> getAuthors() {
        return this.authorRepository.findAll();
    }

    public Author getAuthorById(Long id) {
        Optional<Author> optAuthor = this.authorRepository.findById(id);
        return optAuthor.orElse(null);
    }

    public Author saveAuthor(@Valid Author author) {
        return this.authorRepository.saveAndFlush(author);
    }

    public void deleteAuthorById(Long id) {
        this.authorRepository.deleteById(id);
    }
}
