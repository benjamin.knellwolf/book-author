package ch.bict.book_author.controller;

import ch.bict.book_author.model.Author;
import ch.bict.book_author.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "/author")
@CrossOrigin(origins = {"*"}, allowedHeaders = {"*"})
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    public List<Author> getAuthors() {
        return this.authorService.getAuthors();
    }

    @GetMapping(value = "/{id}")
    public Author getAuthorById(@PathVariable Long id) {
        return this.authorService.getAuthorById(id);
    }

    @PostMapping
    public Author saveAuthor(@RequestBody Author author) {
        return this.authorService.saveAuthor(author);
    }

    @DeleteMapping(value = "/{id}")
    private void deleteAuthorById(@PathVariable Long id) {
        this.authorService.deleteAuthorById(id);
    }
}
