package ch.bict.book_author.model;

import com.sun.istack.NotNull;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.*;
import java.sql.Date;

@Entity
public class Author {

    @Id
    @JsonIgnore
    @SequenceGenerator(name = "sequence_author", sequenceName = "sequence_author", initialValue = 100, allocationSize = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_author")
    private Long id;

    @Column(length = 64, nullable = false)
    @NotNull
    private String firstName;

    @Column(length = 64, nullable = false)
    @NotNull
    private String lastName;

    @Column
    @NotNull
    private Long balance;

    @Column(nullable = false)
    @NotNull
    private Date birthDate;

    @OneToMany
    private List<Book> books;

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
