INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (1, 'John', 'Doe', 2207, '1989-04-6');
INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (2, 'Andreas', 'Knellwolf',534 , '2005-08-25');
INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (3, 'Flat', 'Earther', 1810, '2020-09-07');
INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (4, 'Jeff', 'Kaplan', 12345, '1970-11-12');
INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (5, 'Sheldon', 'Cooper', 3, '1981-07-3');
INSERT INTO `author` (`id`, `first_name`, `last_name`, `balance`, `birth_date`) VALUES (6, 'Gay', 'Bowser', 6969, '2020-07-01');

INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (1, 'Why the earth is flat', 'This book explains why the earth is flat', 1);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (2, 'Why the flat earth is a religion', 'The title is self explanatory', 30);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (3, 'Overwatch Stories', 'Overwatch is a game that has a lot of deep stories for the individual heroes', 25);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (4, 'String Theory simply explained', 'The String theory has been proven true and this book explains it as simple as possible', 130);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (5, 'String Theory advanced version', 'The title is self explanatory', 200);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (6, 'How to capture a princess', 'In this book you learn everything, that can go wrong while capturing a princess', 10);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (7, 'Mechanics of a motorcycle', 'You will learn the basics of every motorcycle part.', 20);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (8, 'My life story', 'I am sharing my amazing life story with you', 60);
INSERT INTO `book` (`id`, `title`, `description`, `price`) VALUES (9, 'The Enemy', 'This is the story of Mario and his brother Luigi', 40);

INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (1, 8);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (2, 2);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (2, 7);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (3, 1);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (4, 3);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (5, 4);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (5, 5);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (6, 6);
INSERT INTO `author_books` (`author_id`, `books_id`) VALUES (6, 9);
